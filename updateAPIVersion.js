const yaml = require('js-yaml');
const fs   = require('fs');


// Get document, or throw exception on error
try {
  const doc = yaml.load(fs.readFileSync('/workspaces/backend/lib/nest-api.yaml', 'utf8'));
  console.log(doc);
  doc.info.version = '0.0.0' 
  console.log(doc.openapi)
  let yamlStr = yaml.dump(doc);

  fs.writeFileSync('/workspaces/backend/lib/nest-api.yaml', yamlStr, 'utf8');
  console.log(doc);
} catch (e) {
  console.log(e);
}