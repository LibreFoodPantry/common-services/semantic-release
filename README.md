# semantic release



## Getting started
1. There is a LibreFoodPantry-level group access token called `GL_TOKEN` that is used for this and is automatically inherited into your project.
    1. You no longer need to do the steps 1-14 below.
    2. If you previously created a personal access token for your project, you should delete it.
>You will need to create a Personal Access Token and add it as a variable in your desired repository.
>    1. In the top-right corner, select your avatar.
>    2. Select Edit profile.
>    3. On the left sidebar, select Access Tokens.
>    4. Enter 'GL_TOKEN' in the name field and leave the expiry date blank.
>    5. Select the 'api' and 'write_repository' scopes.
>    6. Select Create personal access token.
>    7. A string of letters and numbers will pop up under 'Your new personal access token.' This is your access token. Copy it. You may also want to store it somewhere, since you will be unable to see it again.
>    8. Return to your desired repository. 
>    9. On the left sidebar, go to Settings > CI/CD > Variables
>    10. Click 'Add variable'
>    11. Enter 'GL_TOKEN' in the key field.
>    12. Paste your access token into the value field.
>    13. Select 'mask variable.'
>    14. Click 'add variable.'
2. If a .gitlab-ci.yml file does not exist in your repository, create one.
3. Add lines 28 - 38 of the .gitlab-ci.yml file in this repository to the one in your desired repository.
4. If a package.json file does not exist in your repository, create one and copy the entire package.json file from this repository (The 'name' field can be changed depending on the needs of your project). If a package.json file already exists in your repository, add lines 4 - 7 of the package.json file in this repository to your existing one.

## What it Does

Semantic Release will automatically tag a Docker image released to a repository's container registry with a version number. That version number is a semantic versioning number that is calculated based on the number and types of commits to a repository. It will run during the release stage of the CI/CD pipeline, and it will only run on the default branch.
